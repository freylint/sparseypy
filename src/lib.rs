#![allow(dead_code)]

use pyo3::prelude::*;

pub mod components {
    struct Groupinfo;
}

pub mod layout {
    struct ComponentInfo;
    struct Layout;
    struct LayoutBuilder;
    struct LayoutGroup;
}

pub mod query {
    enum Iter {
        Dense = 0,
        Sparse = 1,
    }

    struct DenseIter;
    struct EntityIter;
    struct Include;
    struct IncludeExclude;
    struct SparseIter;
}

pub mod resources {
    struct Res;
    struct ResMut;
    struct Resources;
    struct SyncResources;
}

pub mod storage {
    struct Entity;
    struct Version;
}

pub mod systems {

    enum ScheduleStep {
        Systems = 0,
        LocalSystems = 1,
        LocalFns = 2,
        Barrier = 3,
    }

    enum SystemParamType {
        Entities = 0,
        Comp = 1,
        CompMut = 2,
        Res = 3,
        ResMut = 4,
    }

    struct LocalFn;
    struct LocalSystem;
    struct Schedule;
    // Potentially uneeded
    struct ScheduleBuilder;
    struct system;
}

pub mod world {
    struct Comp;
    struct CompMut;
    struct Entities;
    struct World;
}

/// A Python module implemented in Rust.
#[pymodule]
fn sparseypy(_py: Python, m: &PyModule) -> PyResult<()> {
    Ok(())
}
